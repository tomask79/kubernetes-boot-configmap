## Using of Kubernetes ConfigMap in Spring Boot application ##

Let's take previous Spring Boot Kubernetes example [Spring Boot Kubernetes Deploy](https://bitbucket.org/tomask79/spring-boot-kubernetes-deploy/)
as starter and add [Kubernetes Config Map](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/) into the game.

### Spring Boot Application property ###

First let's add simple boot property to the MVC controller, no big deal:    

```java  
    @RestController
    public class ControllerMVC {

        @Value("${my.system.property:defaultValue}")
        protected String fromSystem;

        @RequestMapping("/sayhello")
        public String mvcTest() {
             return "I'm saying hello to Kubernetes with system property "+fromSystem+" !";
        }
    }
```

and define the my.system.property into the **application.properties**:	

```java
    server.port=8081 
    my.system.property=fromFile
```
to test that property is loaded run:

* mvn clean install
* tomask79:kubernetes-configmap tomask79$ java -jar target/demo-0.0.1-SNAPSHOT.jar
* tomask79:kubernetes-configmap tomask79$ curl http://localhost:8081/sayhello

output should be:    

`
    I'm saying hello to Kubernetes with system property fromFile 
`
### Consume Kubernetes ConfigMap in Spring Boot ###

Nice way of consuming the Kubernetes configmap in Spring Boot is **through environment variables**.
So let's define environment variable my.system.property with value from configmap **app-config** 
in order to override value from application.properties:

First let's define the configmap app-config with key=my.system.property and value "fromAnsible":    

`
    kubectl create configmap app-config --from-literal=my.system.property=updatedFromAnsible
`

to see that configmap is well defined, run:    

`
kubectl describe configmap app-config
`

output should be:    

	Name:         app-config
	Namespace:    default    
	Labels:       none    
	Annotations:  none    
	Data    
	====    
	my.system.property:    
	----    
	updatedFromAnsible    
	Events:  none

Okay, configmap is ready so let the kubernetes know about it and **define environment  
variable my.system.property reading the key with same name from configmap app-config**.    
    

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: test-app
spec:
  replicas: 1
  template: 
    metadata:
      labels:
        app: test-app
    spec:
      containers:
      - name: test-app
        image: test-controller:1.0-SNAPSHOT
        env:
          # Define the environment variable
          - name: my.system.property
            valueFrom:
              configMapKeyRef:
                # The ConfigMap containing the value you want to assign to my.system.property
                name: app-config
                # Specify the key associated with the value
                key: my.system.property
        ports:
        - containerPort: 8081
```
Now upload the Deployment manifest to kubernetes with kubernetes service    
(this is desired output, for howto upload see [previous repo](http://bitbucket.org/tomask79/spring-boot-kubernetes-deploy/)):    


        tomask79:kubernetes-configmap tomask79$ kubectl get deployments
        NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
        hello-minikube   1         1         1            1           26d
        test-app         1         1         1            1           2h
        tomask79:kubernetes-configmap tomask79$ kubectl get pods
        NAME                             READY     STATUS    RESTARTS   AGE
        hello-minikube-6c47c66d8-gzvgc   1/1       Running   5          26d
        test-app-745ff9546c-hrswc        1/1       Running   0          1h
        tomask79:kubernetes-configmap tomask79$ kubectl get services
        NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
        kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          26d
        test-app     NodePort    10.105.171.8   <none>        8081:30615/TCP   2h

since kubernetes service runs at:

    tomask79:kubernetes-configmap tomask79$ minikube service test-app --url
    http://192.168.99.100:30615
    tomask79:kubernetes-configmap tomask79$ 


hit the returned URL:

    tomask79:kubernetes-configmap tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property updatedFromAnsible !
    
Yes, we have overriden the spring property my.system.property via Kubernetes configmap    
by injecting the property as environment variable, because env.variables have bigger priority    
in Spring Boot then content of application.property file.

### Updating the configMap ###

Let's play with configmap a little bit. What if we want to change the map value.    
Easiest way of howto do it is by deleting map and create a new one.

    tomask79:kubernetes-configmap tomask79$ kubectl delete configmap app-config
    configmap "app-config" deleted
    tomask79:kubernetes-configmap tomask79$ kubectl create configmap app-config --from-literal=my.system.property=changedValue
    configmap "app-config" created
    tomask79:kubernetes-configmap tomask79$ tomask79:kubernetes-configmap tomask79$ kubectl delete configmap app-config

now curl the service again:

    tomask79:kubernetes-configmap tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property updatedFromAnsible !

new value of configmap **wasn't reflected** and we're having still the old one.    

To see the actualized value of configmap we need to restart the POD. Since we use deployment
where kubernetes is asked always run one replica we just need to delete the POD. New instance    
will run eventually.

    tomask79:kubernetes-configmap tomask79$ kubectl delete pod test-app-745ff9546c-hrswc
    pod "test-app-745ff9546c-hrswc" deleted
    tomask79:kubernetes-configmap tomask79$ kubectl get pods
    NAME                             READY     STATUS    RESTARTS   AGE
    hello-minikube-6c47c66d8-gzvgc   1/1       Running   5          27d
    test-app-745ff9546c-t92hb        1/1       Running   0          31s
    tomask79:kubernetes-configmap tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property changedValue !

we see actual value!    

I hope you will find my demo usefull.

Best regards

Tomas






